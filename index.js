import Track from './Track.js';

class App {
    constructor() {
        this.songs = [];
        this.currentTrack = null;

        // DOM Elements
        this.elTrackList = document.getElementById("track-list");
        this.elSongName = document.getElementById("current-track-name");

        this.elTrackLast = document.getElementById("previousTrackButton");
        this.elTrackPlay = document.getElementById("playButton");
        this.elTrackNext = document.getElementById("nextTrackButton");
        this.elTrackElapsed = document.getElementById("duration-elapsed");
        this.elTrackElapsedNow = document.getElementById("duration-now");
        this.elTrackElapsedEnd = document.getElementById("duration-end");

        // Dynamic DOM Elements
        this.elActiveTrack = null;
        this.elTrackItems = [];

        // Button Click: Play button
        this.elTrackPlay.onclick = () => {
            if (this.currentTrack == null)
            {
                // Activate the first entry
                this.OnTrackClick(this.elTrackItems[0]);
            } else {
                if (!this.currentTrack.playing)
                {
                    this.currentTrack.play();
                } else {
                    this.currentTrack.pause();
                }
            }

            this.RenderPlaybar();
        }

        // Button Click: Previous Track button
        this.elTrackLast.onclick = () => {
            let currentId = this.currentTrack != null ? this.currentTrack.id : -1;
            let nextId = currentId - 1;

            if (nextId < 0) {
                nextId = this.songs.length - 1;
            }

            let track = this.elTrackItems[nextId];
            this.OnTrackClick(track);
        }

        // Button Click: Next Track button
        this.elTrackNext.onclick = () => {
            let currentId = this.currentTrack != null ? this.currentTrack.id : -1;
            let nextId = currentId + 1;

            if (nextId > (this.songs.length - 1)) {
                nextId = 0;
            }

            let track = this.elTrackItems[nextId];
            this.OnTrackClick(track);
        }
    }

    // Add a new track
    AddTrack(track) {        
        this.songs.push(track);

        this.RenderList();
    }

    // Convert seconds to minutes:seconds format
    FormatTime(duration) {
        let rounded = Math.round(duration);
        let dMin = Math.round(rounded / 60) || 0;
        let dSec = rounded - dMin * 60;
        if (dSec < 0) {
            dMin -= 1;
            dSec = 60 + dSec;
        }

        return `${dMin}:${dSec < 10 ? '0' : ''}${dSec}`;
    }

    // Recursive animation (The elapsed time bar)
    Step() {
        let elapsed = this.currentTrack.elapsed();
        let duration = this.currentTrack.duration;
        let percentage = elapsed / duration;

        if (!isNaN(percentage))
        {
            this.elTrackElapsed.style.width = `${percentage * 100}%`;

            this.elTrackElapsedNow.innerText = this.FormatTime(elapsed);
            this.elTrackElapsedEnd.innerText = this.FormatTime(duration);
        }

        if (this.currentTrack.playing) {
            window.requestAnimationFrame(() => this.Step());
        }
    }

    // When a track item is clicked, play the song
    OnTrackClick(trackItem) {
        // Stop previous track
        if (this.currentTrack != null) {
            this.currentTrack.stop();
        }

        // Setup and play next track
        let trackId = trackItem.value;
        let track = this.songs[trackId];

        if (this.elActiveTrack != null) {
            this.elActiveTrack.classList.remove("active");
        }

        this.elActiveTrack = trackItem;
        this.elActiveTrack.classList.add("active");
        this.currentTrack = track;

        this.currentTrack.play();
        this.RenderPlaybar();
        window.requestAnimationFrame(() => this.Step());
    }

    // Update the information for the bottom navigation bar
    RenderPlaybar() {
        this.elSongName.innerText = this.currentTrack.name;

        let artistItem = document.createElement("small");
        artistItem.innerHTML = `\tby <b>${this.currentTrack.artist}</b>`;

        let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;

        if (!isMobile) {
            this.elSongName.appendChild(artistItem);
        }


        this.elTrackPlay.innerHTML = !this.currentTrack.playing
            ? `<img src="/Icons/play-fill.svg" alt="" title="Play button">`
            : `<img src="/Icons/pause-fill.svg" alt="" title="Pause button">`;
    }

    // Update the list with new details
    RenderList() {
        // Empty the track list
        this.elTrackItems = [];
        while (this.elTrackList.firstChild) {
            this.elTrackList.removeChild(this.elTrackList.firstChild);
        }
        
        // Repopulate the list
        this.songs.forEach((song, index) => {
            let item = document.createElement('li');

            let artistItem = document.createElement('small');
            artistItem.innerText = song.artist;
            artistItem.setAttribute("id", "track-artist-item");

            item.setAttribute("id", "track-item");
            item.setAttribute("class", "list-group-item");
            item.setAttribute("value", index);
            item.innerHTML = `${song.name} — `;

            item.appendChild(artistItem);

            this.elTrackList.appendChild(item);
            this.elTrackItems.push(item);
            
            // Bind item to event
            item.onclick = (e) => {
                this.OnTrackClick(e.target);
            };
        });
    }
}

let instance = new App();

let songs = [
    {
        name: "God's Plan",
        artist: "Drake",
        url: "Drake - God's Plan.mp3"
    },
    {
        name: "Toosie Slide",
        artist: "Drake",
        url: "Drake - Toosie Slide.mp3"
    },
    {
        name: "Chicago Freestyle",
        artist: "Drake feat. Giveon",
        url: "Drake feat. Giveon - Chicago Freestyle.mp3"
    },
    {
        name: "One Dance",
        artist: "Drake feat. WizKid, Kyle",
        url: "Drake feat. WizKid, Kyla - One Dance.mp3"
    },
    {
        name: "Forrest Gump",
        artist: "Frank Ocean",
        url: "Frank Ocean - Forrest Gump.mp3"
    },
    {
        name: "Ivy",
        artist: "Frank Ocean",
        url: "Frank Ocean - Ivy.mp3"
    },
    {
        name: "Lost",
        artist: "Frank Ocean",
        url: "Frank Ocean - Lost.mp3"
    },
    {
        name: "Nights",
        artist: "Frank Ocean",
        url: "Frank Ocean - Nights.mp3"
    },
    {
        name: "Pink + White",
        artist: "Frank Ocean",
        url: "Frank Ocean - Pink + White.mp3"
    },
    {
        name: "Self Control",
        artist: "Frank Ocean",
        url: "Frank Ocean - Self Control.mp3"
    },
    {
        name: "Solo",
        artist: "Frank Ocean",
        url: "Frank Ocean - Solo.mp3"
    },
]

songs.forEach((song, id) => {
    let track = new Track(song.name, `http://localhost:5500/Tracks/${song.url}`);
    track.setArtist(song.artist);
    track.setId(id);
    track.setLastId(songs.length - 1);

    instance.AddTrack(track);
});