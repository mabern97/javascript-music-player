export default class Track {
    constructor(trackName, trackURL) {
        this.name = trackName;
        this.artist = trackName;
        this.url = trackURL;

        this.id = 0;
        this.lastId = 0;

        this.duration = 0;

        this.howl = null;
        this.playing = false;
    }

    setId(id) {
        this.id = id;
    }

    setLastId(id) {
        this.lastId = id;
    }

    setArtist(name) {
        this.artist = name;
    }

    stop() {
        if (!this.howl) return;

        this.howl.stop();
        this.howl.unload();
        this.playing = false;
    }

    play() {
        if (!this.howl) {
            //return;
            this.howl = new Howl({
                src: this.url,
                onplay: () => {
                    this.duration = this.howl.duration();
                }
            });
        }

        this.howl.play();
        this.playing = true;
    }

    elapsed() {
        return this.howl.seek();
    }

    pause() {
        if (!this.howl)
            return;

        this.howl.pause();
        this.playing = false;
    }
}